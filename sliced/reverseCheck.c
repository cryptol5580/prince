#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>
//uint16_t mLayer(uint64_t state);
//uint64_t sLayer(uint64_t state);
//uint64_t isLayer(uint64_t state);
void shiftrow();
void ishiftrow();
void mpLayer();
void sLayer();
void isLayer();

int16_t Prince(uint64_t prectext);

#define CipherOut(ctext) printf("0x%016"PRIx64"\n",(ctext))


// 8x16bit registers to store two 64-bit states
// one 64-bit state is 16x(4-bit nibble) (index 0-15)
// each register contains 4 nibbles, 2 for each state
uint16_t reg0=0; //nibble 0 and 12 of state0 and state1
uint16_t reg1=0; //nibble 4 and 8 of state0 and state1
uint16_t reg2=0; //nibble 1 and 13 of state0 and state1
uint16_t reg3=0; //nibble 5 and 9 of state0 and state1
uint16_t reg4=0; //nibble 2 and 14 of state0 and state1
uint16_t reg5=0; //nibble 6 and 10 of state0 and state1
uint16_t reg6=0; //nibble 3 and 15 of state0 and state1
uint16_t reg7=0; //nibble 7 and 11 of state0 and state1

uint16_t key00=0;
uint16_t key01=0;
uint16_t key02=0;
uint16_t key03=0;
uint16_t key04=0;
uint16_t key05=0;
uint16_t key06=0;
uint16_t key07=0;

uint16_t key0p0=0;
uint16_t key0p1=0;
uint16_t key0p2=0;
uint16_t key0p3=0;
uint16_t key0p4=0;
uint16_t key0p5=0;
uint16_t key0p6=0;
uint16_t key0p7=0;

uint16_t key10=0;
uint16_t key11=0;
uint16_t key12=0;
uint16_t key13=0;
uint16_t key14=0;
uint16_t key15=0;
uint16_t key16=0;
uint16_t key17=0;

const uint16_t RC0[12] = {
		0x0000,	0x9494, 0x9090, 0xe9e9, 0x8787, 0x4c4c,
		0x8181, 0x4a4a, 0x2424, 0x5d5d, 0x5959, 0xcdcd
};
const uint16_t RC1[12] = {
		0x0000, 0xe0e0, 0x2f2f, 0x8e8e, 0x6060, 0xf9f9,
		0x8585, 0x1c1c, 0xf2f2, 0x5353, 0x9c9c, 0x7c7c
};
const uint16_t RC2[12] = {
		0x0000, 0x1414, 0x0d0d, 0x2828, 0x2727, 0x5656,
		0xfbfb, 0x8a8a, 0x8585, 0xa0a0, 0xb9b9, 0xadad
};
const uint16_t RC3[12] = {
		0x0000, 0x2727, 0x2929, 0x9494, 0xeded, 0xcece,
		0x7979, 0x5a5a, 0x2323, 0x9e9e, 0x9090, 0xb7b7
};
const uint16_t RC4[12] = {
		0x0000, 0x3333, 0x4141, 0x8c8c, 0x5353, 0xecec,
		0xecec, 0x5353, 0x8c8c, 0x4141, 0x3333, 0x0000
};
const uint16_t RC5[12] = {
		0x0000, 0xa3a3, 0x8989, 0xacac, 0x1818, 0x6464,
		0xfdfd, 0x8181, 0x3535, 0x1010, 0x3a3a, 0x9999
};
const uint16_t RC6[12] = {
		0x0000,  0x1717, 0xa3a3, 0x0606, 0x4141, 0xb0b0,
		0x7575, 0x8484, 0xc3c3, 0x6666, 0xd2d2, 0xc5c5
};
const uint16_t RC7[12] = {
		0x0000, 0x8080, 0x3232, 0xfefe, 0x2323, 0x6363,
		0x4f4f, 0x0f0f, 0xd2d2, 0x1e1e, 0xacac, 0x2c2c
};

const uint16_t sbox[256] = {
		0xbb, 0xbf, 0xb3, 0xb2, 0xba, 0xbc, 0xb9, 0xb1, 0xb6, 0xb7, 0xb8, 0xb0, 0xbe, 0xb5, 0xbd, 0xb4,
		0xfb, 0xff, 0xf3, 0xf2, 0xfa, 0xfc, 0xf9, 0xf1, 0xf6, 0xf7, 0xf8, 0xf0, 0xfe, 0xf5, 0xfd, 0xf4,
		0x3b, 0x3f, 0x33, 0x32, 0x3a, 0x3c, 0x39, 0x31, 0x36, 0x37, 0x38, 0x30, 0x3e, 0x35, 0x3d, 0x34,
		0x2b, 0x2f, 0x23, 0x22, 0x2a, 0x2c, 0x29, 0x21, 0x26, 0x27, 0x28, 0x20, 0x2e, 0x25, 0x2d, 0x24,
		0xab, 0xaf, 0xa3, 0xa2, 0xaa, 0xac, 0xa9, 0xa1, 0xa6, 0xa7, 0xa8, 0xa0, 0xae, 0xa5, 0xad, 0xa4,
		0xcb, 0xcf, 0xc3, 0xc2, 0xca, 0xcc, 0xc9, 0xc1, 0xc6, 0xc7, 0xc8, 0xc0, 0xce, 0xc5, 0xcd, 0xc4,
		0x9b, 0x9f, 0x93, 0x92, 0x9a, 0x9c, 0x99, 0x91, 0x96, 0x97, 0x98, 0x90, 0x9e, 0x95, 0x9d, 0x94,
		0x1b, 0x1f, 0x13, 0x12, 0x1a, 0x1c, 0x19, 0x11, 0x16, 0x17, 0x18, 0x10, 0x1e, 0x15, 0x1d, 0x14,
		0x6b, 0x6f, 0x63, 0x62, 0x6a, 0x6c, 0x69, 0x61, 0x66, 0x67, 0x68, 0x60, 0x6e, 0x65, 0x6d, 0x64,
		0x7b, 0x7f, 0x73, 0x72, 0x7a, 0x7c, 0x79, 0x71, 0x76, 0x77, 0x78, 0x70, 0x7e, 0x75, 0x7d, 0x74,
		0x8b, 0x8f, 0x83, 0x82, 0x8a, 0x8c, 0x89, 0x81, 0x86, 0x87, 0x88, 0x80, 0x8e, 0x85, 0x8d, 0x84,
		0x0b, 0x0f, 0x03, 0x02, 0x0a, 0x0c, 0x09, 0x01, 0x06, 0x07, 0x08, 0x00, 0x0e, 0x05, 0x0d, 0x04,
		0xeb, 0xef, 0xe3, 0xe2, 0xea, 0xec, 0xe9, 0xe1, 0xe6, 0xe7, 0xe8, 0xe0, 0xee, 0xe5, 0xed, 0xe4,
		0x5b, 0x5f, 0x53, 0x52, 0x5a, 0x5c, 0x59, 0x51, 0x56, 0x57, 0x58, 0x50, 0x5e, 0x55, 0x5d, 0x54,
		0xdb, 0xdf, 0xd3, 0xd2, 0xda, 0xdc, 0xd9, 0xd1, 0xd6, 0xd7, 0xd8, 0xd0, 0xde, 0xd5, 0xdd, 0xd4,
		0x4b, 0x4f, 0x43, 0x42, 0x4a, 0x4c, 0x49, 0x41, 0x46, 0x47, 0x48, 0x40, 0x4e, 0x45, 0x4d, 0x44};

const uint16_t isbox[256] = {
		0xbb, 0xb7, 0xb3, 0xb2, 0xbf, 0xbd, 0xb8, 0xb9, 0xba, 0xb6, 0xb4, 0xb0, 0xb5, 0xbe, 0xbc, 0xb1,
		0x7b, 0x77, 0x73, 0x72, 0x7f, 0x7d, 0x78, 0x79, 0x7a, 0x76, 0x74, 0x70, 0x75, 0x7e, 0x7c, 0x71,
		0x3b, 0x37, 0x33, 0x32, 0x3f, 0x3d, 0x38, 0x39, 0x3a, 0x36, 0x34, 0x30, 0x35, 0x3e, 0x3c, 0x31,
		0x2b, 0x27, 0x23, 0x22, 0x2f, 0x2d, 0x28, 0x29, 0x2a, 0x26, 0x24, 0x20, 0x25, 0x2e, 0x2c, 0x21,
		0xfb, 0xf7, 0xf3, 0xf2, 0xff, 0xfd, 0xf8, 0xf9, 0xfa, 0xf6, 0xf4, 0xf0, 0xf5, 0xfe, 0xfc, 0xf1,
		0xdb, 0xd7, 0xd3, 0xd2, 0xdf, 0xdd, 0xd8, 0xd9, 0xda, 0xd6, 0xd4, 0xd0, 0xd5, 0xde, 0xdc, 0xd1,
		0x8b, 0x87, 0x83, 0x82, 0x8f, 0x8d, 0x88, 0x89, 0x8a, 0x86, 0x84, 0x80, 0x85, 0x8e, 0x8c, 0x81,
		0x9b, 0x97, 0x93, 0x92, 0x9f, 0x9d, 0x98, 0x99, 0x9a, 0x96, 0x94, 0x90, 0x95, 0x9e, 0x9c, 0x91,
		0xab, 0xa7, 0xa3, 0xa2, 0xaf, 0xad, 0xa8, 0xa9, 0xaa, 0xa6, 0xa4, 0xa0, 0xa5, 0xae, 0xac, 0xa1,
		0x6b, 0x67, 0x63, 0x62, 0x6f, 0x6d, 0x68, 0x69, 0x6a, 0x66, 0x64, 0x60, 0x65, 0x6e, 0x6c, 0x61,
		0x4b, 0x47, 0x43, 0x42, 0x4f, 0x4d, 0x48, 0x49, 0x4a, 0x46, 0x44, 0x40, 0x45, 0x4e, 0x4c, 0x41,
		0x0b, 0x07, 0x03, 0x02, 0x0f, 0x0d, 0x08, 0x09, 0x0a, 0x06, 0x04, 0x00, 0x05, 0x0e, 0x0c, 0x01,
		0x5b, 0x57, 0x53, 0x52, 0x5f, 0x5d, 0x58, 0x59, 0x5a, 0x56, 0x54, 0x50, 0x55, 0x5e, 0x5c, 0x51,
		0xeb, 0xe7, 0xe3, 0xe2, 0xef, 0xed, 0xe8, 0xe9, 0xea, 0xe6, 0xe4, 0xe0, 0xe5, 0xee, 0xec, 0xe1,
		0xcb, 0xc7, 0xc3, 0xc2, 0xcf, 0xcd, 0xc8, 0xc9, 0xca, 0xc6, 0xc4, 0xc0, 0xc5, 0xce, 0xcc, 0xc1,
		0x1b, 0x17, 0x13, 0x12, 0x1f, 0x1d, 0x18, 0x19, 0x1a, 0x16, 0x14, 0x10, 0x15, 0x1e, 0x1c, 0x11};

#define ComputeK1Prime(k0) (((k0)>>1 | (k0)<<63 ) ^ (k0)>>63)

void InstallStates(uint64_t ptext1, uint64_t ptext2)
{
	//construct initial state
	reg0 = ((uint16_t)(ptext2 >>36 & 0xf000)) | ((uint16_t)(ptext2<<8 & 0xf00)) | ((uint16_t)(ptext1 >>44 & 0xf0)) | ((uint16_t)(ptext1 & 0xf));
	reg1 = ((uint16_t)(ptext2 >>20 & 0xf000)) | ((uint16_t)(ptext2>>8 & 0xf00)) | ((uint16_t)(ptext1 >>28 & 0xf0)) | ((uint16_t)(ptext1>>16 & 0xf));
	reg2 = ((uint16_t)(ptext2 >>40 & 0xf000)) | ((uint16_t)(ptext2<<4 & 0xf00)) | ((uint16_t)(ptext1 >>48 & 0xf0)) | ((uint16_t)(ptext1>>4 & 0xf));
	reg3 = ((uint16_t)(ptext2 >>24 & 0xf000)) | ((uint16_t)(ptext2>>12 & 0xf00)) | ((uint16_t)(ptext1 >>32 & 0xf0)) | ((uint16_t)(ptext1>>20 & 0xf));
	reg4 = ((uint16_t)(ptext2 >>44 & 0xf000)) | ((uint16_t)(ptext2 & 0xf00)) | ((uint16_t)(ptext1 >>52 & 0xf0)) | ((uint16_t)(ptext1>>8 & 0xf));
	reg5 = ((uint16_t)(ptext2 >>28 & 0xf000)) | ((uint16_t)(ptext2>>16 & 0xf00)) | ((uint16_t)(ptext1 >>36 & 0xf0)) | ((uint16_t)(ptext1>>24 & 0xf));
	reg6 = ((uint16_t)(ptext2 >>48 & 0xf000)) | ((uint16_t)(ptext2>>4 & 0xf00)) | ((uint16_t)(ptext1 >>56 & 0xf0)) | ((uint16_t)(ptext1>>12 & 0xf));
	reg7 = ((uint16_t)(ptext2 >>32 & 0xf000)) | ((uint16_t)(ptext2>>20 & 0xf00)) | ((uint16_t)(ptext1 >>40 & 0xf0)) | ((uint16_t)(ptext1>>28 & 0xf));
}

void InstallKeys(uint64_t key0, uint64_t key0p,uint64_t key1)
{
	uint64_t key1a = key1 ^ 0xc0ac29b7c97c50dd; // k0 ^ alpha
	uint16_t tmp;

	//construct key0
	key00 = ((uint16_t)(key0 >>44 & 0xf0)) | ((uint16_t)(key0 & 0xf));
	key01 = ((uint16_t)(key0 >>28 & 0xf0)) | ((uint16_t)(key0 >>16 & 0xf));
	key02 = ((uint16_t)(key0 >>48 & 0xf0)) | ((uint16_t)(key0 >>4 & 0xf));
	key03 = ((uint16_t)(key0 >>32 & 0xf0)) | ((uint16_t)(key0 >>20 & 0xf));
	key04 = ((uint16_t)(key0 >>52 & 0xf0)) | ((uint16_t)(key0 >>8 & 0xf));
	key05 = ((uint16_t)(key0 >>36 & 0xf0)) | ((uint16_t)(key0 >>24 & 0xf));
	key06 = ((uint16_t)(key0 >>56 & 0xf0)) | ((uint16_t)(key0 >>12 & 0xf));
	key07 = ((uint16_t)(key0 >>40 & 0xf0)) | ((uint16_t)(key0 >>28 & 0xf));

	//construct key0p
	key0p0 = ((uint16_t)(key0p >>44 & 0xf0)) | ((uint16_t)(key0p & 0xf));
	key0p1 = ((uint16_t)(key0p >>28 & 0xf0)) | ((uint16_t)(key0p >>16 & 0xf));
	key0p2 = ((uint16_t)(key0p >>48 & 0xf0)) | ((uint16_t)(key0p >>4 & 0xf));
	key0p3 = ((uint16_t)(key0p >>32 & 0xf0)) | ((uint16_t)(key0p >>20 & 0xf));
	key0p4 = ((uint16_t)(key0p >>52 & 0xf0)) | ((uint16_t)(key0p >>8 & 0xf));
	key0p5 = ((uint16_t)(key0p >>36 & 0xf0)) | ((uint16_t)(key0p >>24 & 0xf));
	key0p6 = ((uint16_t)(key0p >>56 & 0xf0)) | ((uint16_t)(key0p >>12 & 0xf));
	key0p7 = ((uint16_t)(key0p >>40 & 0xf0)) | ((uint16_t)(key0p >>28 & 0xf));

	tmp = key00;
	key00 |= key0p0<<8;
	key0p0 |= tmp<<8;

	tmp = key01;
	key01 |= key0p1<<8;
	key0p1 |= tmp<<8;

	tmp = key02;
	key02 |= key0p2<<8;
	key0p2 |= tmp<<8;

	tmp = key03;
	key03 |= key0p3<<8;
	key0p3 |= tmp<<8;

	tmp = key04;
	key04 |= key0p4<<8;
	key0p4 |= tmp<<8;

	tmp = key05;
	key05 |= key0p5<<8;
	key0p5 |= tmp<<8;

	tmp = key06;
	key06 |= key0p6<<8;
	key0p6 |= tmp<<8;

	tmp = key07;
	key07 |= key0p7<<8;
	key0p7 |= tmp<<8;

	//construct key1
	key10 = ((uint16_t)(key1 >>44 & 0xf0)) | ((uint16_t)(key1 & 0xf));
	tmp = ((uint16_t)(key1a >>44 & 0xf0)) | ((uint16_t)(key1a & 0xf));
	key10 |= tmp<<8;

	key11 = ((uint16_t)(key1 >>28 & 0xf0)) | ((uint16_t)(key1 >>16 & 0xf));
	tmp = ((uint16_t)(key1a >>28 & 0xf0)) | ((uint16_t)(key1a >>16 & 0xf));
	key11 |= tmp<<8;

	key12 = ((uint16_t)(key1 >>48 & 0xf0)) | ((uint16_t)(key1 >>4 & 0xf));
	tmp = ((uint16_t)(key1a >>48 & 0xf0)) | ((uint16_t)(key1a >>4 & 0xf));
	key12 |= tmp<<8;

	key13 = ((uint16_t)(key1 >>32 & 0xf0)) | ((uint16_t)(key1 >>20 & 0xf));
	tmp = ((uint16_t)(key1a >>32 & 0xf0)) | ((uint16_t)(key1a >>20 & 0xf));
	key13 |= tmp<<8;

	key14 = ((uint16_t)(key1 >>52 & 0xf0)) | ((uint16_t)(key1 >>8 & 0xf));
	tmp = ((uint16_t)(key1a >>52 & 0xf0)) | ((uint16_t)(key1a >>8 & 0xf));
	key14 |= tmp<<8;

	key15 = ((uint16_t)(key1 >>36 & 0xf0)) | ((uint16_t)(key1 >>24 & 0xf));
	tmp = ((uint16_t)(key1a >>36 & 0xf0)) | ((uint16_t)(key1a >>24 & 0xf));
	key15 |= tmp<<8;

	key16 = ((uint16_t)(key1 >>56 & 0xf0)) | ((uint16_t)(key1 >>12 & 0xf));
	tmp = ((uint16_t)(key1a >>56 & 0xf0)) | ((uint16_t)(key1a >>12 & 0xf));
	key16 |= tmp<<8;

	key17 = ((uint16_t)(key1 >>40 & 0xf0)) | ((uint16_t)(key1 >>28 & 0xf));
	tmp = ((uint16_t)(key1a >>40 & 0xf0)) | ((uint16_t)(key1a >>28 & 0xf));
	key17 |= tmp<<8;

}

//void PostCompute(uint64_t *ptext1, uint64_t *ptext2)
uint64_t RecoverStates()
{
	// recover states
	uint64_t p1;
//	uint64_t p2;
	p1 = (((uint64_t)reg0)&0xf) |
			((((uint64_t)reg2)&0xf)<<4) |
			((((uint64_t)reg4)&0xf)<<8) |
			((((uint64_t)reg6)&0xf)<<12) |
			((((uint64_t)reg1)&0xf)<<16) |
			((((uint64_t)reg3)&0xf)<<20) |
			((((uint64_t)reg5)&0xf)<<24) |
			((((uint64_t)reg7)&0xf)<<28) |
			((((uint64_t)reg1)&0xf0)<<28) |
			((((uint64_t)reg3)&0xf0)<<32) |
			((((uint64_t)reg5)&0xf0)<<36) |
			((((uint64_t)reg7)&0xf0)<<40) |
			((((uint64_t)reg0)&0xf0)<<44) |
			((((uint64_t)reg2)&0xf0)<<48) |
			((((uint64_t)reg4)&0xf0)<<52) |
			((((uint64_t)reg6)&0xf0)<<56);

//	p2 = (((uint64_t)reg0)&0xf00)>>8 |
//			((((uint64_t)reg2)&0xf00)>>4) |
//			((((uint64_t)reg4)&0xf00)) |
//			((((uint64_t)reg6)&0xf00)<<4) |
//			((((uint64_t)reg1)&0xf00)<<8) |
//			((((uint64_t)reg3)&0xf00)<<12) |
//			((((uint64_t)reg5)&0xf00)<<16) |
//			((((uint64_t)reg7)&0xf00)<<20) |
//			((((uint64_t)reg1)&0xf000)<<20) |
//			((((uint64_t)reg3)&0xf000)<<24) |
//			((((uint64_t)reg5)&0xf000)<<28) |
//			((((uint64_t)reg7)&0xf000)<<32) |
//			((((uint64_t)reg0)&0xf000)<<36) |
//			((((uint64_t)reg2)&0xf000)<<40) |
//			((((uint64_t)reg4)&0xf000)<<44) |
//			((((uint64_t)reg6)&0xf000)<<48);
	return p1;
}

int main(int argc, char **argv){
	uint64_t ptext[] = {0x0000000000000000, 0xffffffffffffffff, 0x0123456789abcdef, 0xfedcba9876543210};// 0x818665aa0d02dfda;
	uint16_t ptextLen = 4;

	uint64_t key1 =0x0; //^0xc0ac29b7c97c50dd;
	uint64_t key0 =0x0;
	uint64_t key0p = ComputeK1Prime(key0);

	uint64_t ctext =0;
	InstallKeys(key0p, key0, key1);
	int i;
	for(i=0;i<ptextLen;i++){
		InstallStates(ptext[i],ctext);
		if (Prince(ctext))
		{
			printf("Data Inconsistency Detected at Runtime!!\n");
			return -1;
		}
		ctext = RecoverStates();
	}
	return 0;
}


int16_t Prince(uint64_t prectext)
{
	static uint16_t flag = 0;
	static uint16_t iState0 = 0; //to store intermediate state for checking
	static uint16_t iState1 = 0;
	static uint16_t iState2 = 0;
	static uint16_t iState3 = 0;
	static uint16_t iState4 = 0;
	static uint16_t iState5 = 0;
	static uint16_t iState6 = 0;
	static uint16_t iState7 = 0;

	int i=0;
	//round0
	reg0 = reg0 ^ key00 ^ key10;
	reg1 = reg1 ^ key01 ^ key11;
	reg2 = reg2 ^ key02 ^ key12;
	reg3 = reg3 ^ key03 ^ key13;
	reg4 = reg4 ^ key04 ^ key14;
	reg5 = reg5 ^ key05 ^ key15;
	reg6 = reg6 ^ key06 ^ key16;
	reg7 = reg7 ^ key07 ^ key17;

	for(i=1;i<6;i++)
	{
		sLayer();
		mpLayer();
		shiftrow();
		reg0 = reg0 ^ RC0[i] ^ key10;
		reg1 = reg1 ^ RC1[i] ^ key11;
		reg2 = reg2 ^ RC2[i] ^ key12;
		reg3 = reg3 ^ RC3[i] ^ key13;
		reg4 = reg4 ^ RC4[i] ^ key14;
		reg5 = reg5 ^ RC5[i] ^ key15;
		reg6 = reg6 ^ RC6[i] ^ key16;
		reg7 = reg7 ^ RC7[i] ^ key17;
	}

	/*************special sLayer to check data consistency ***************************/
	if (flag &
			((sbox[(reg0 & 0xff00)>>8] ^ iState0)|
			(sbox[(reg1 & 0xff00)>>8] ^ iState1)|
			(sbox[(reg2 & 0xff00)>>8] ^ iState2)|
			(sbox[(reg3 & 0xff00)>>8] ^ iState3)|
			(sbox[(reg4 & 0xff00)>>8] ^ iState4)|
			(sbox[(reg5 & 0xff00)>>8] ^ iState5)|
			(sbox[(reg6 & 0xff00)>>8] ^ iState6)|
			(sbox[(reg7 & 0xff00)>>8] ^ iState7)))
	{
		flag =0;
		return 1;
	}

	CipherOut(prectext); // data ok, output ciphertext of previous round. The additional latency is half of the whole cipher

	reg0 = (iState0<<8) | sbox[(reg0&0xff)];
	reg1 = (iState1<<8) | sbox[(reg1&0xff)];
	reg2 = (iState2<<8) | sbox[(reg2&0xff)];
	reg3 = (iState3<<8) | sbox[(reg3&0xff)];
	reg4 = (iState4<<8) | sbox[(reg4&0xff)];
	reg5 = (iState5<<8) | sbox[(reg5&0xff)];
	reg6 = (iState6<<8) | sbox[(reg6&0xff)];
	reg7 = (iState7<<8) | sbox[(reg7&0xff)];
	flag=1;


	/*********************  End of special sLayer  **********************************/

	mpLayer();

	/*************To store intermediate state **********************/
	iState0 = reg0 & 0xff;
	iState1 = reg1 & 0xff;
	iState2 = reg2 & 0xff;
	iState3 = reg3 & 0xff;
	iState4 = reg4 & 0xff;
	iState5 = reg5 & 0xff;
	iState6 = reg6 & 0xff;
	iState7 = reg7 & 0xff;

	isLayer();

	for(i=6;i<11;i++)
	{
		reg0 = reg0 ^ RC0[i] ^ key10;
		reg1 = reg1 ^ RC1[i] ^ key11;
		reg2 = reg2 ^ RC2[i] ^ key12;
		reg3 = reg3 ^ RC3[i] ^ key13;
		reg4 = reg4 ^ RC4[i] ^ key14;
		reg5 = reg5 ^ RC5[i] ^ key15;
		reg6 = reg6 ^ RC6[i] ^ key16;
		reg7 = reg7 ^ RC7[i] ^ key17;

		ishiftrow();
		mpLayer();
		isLayer();
	}

	reg0 = reg0 ^ key0p0 ^ key10 ^ RC0[11];
	reg1 = reg1 ^ key0p1 ^ key11 ^ RC1[11];
	reg2 = reg2 ^ key0p2 ^ key12 ^ RC2[11];
	reg3 = reg3 ^ key0p3 ^ key13 ^ RC3[11];
	reg4 = reg4 ^ key0p4 ^ key14 ^ RC4[11];
	reg5 = reg5 ^ key0p5 ^ key15 ^ RC5[11];
	reg6 = reg6 ^ key0p6 ^ key16 ^ RC6[11];
	reg7 = reg7 ^ key0p7 ^ key17 ^ RC7[11];
	return 0;
}



void sLayer(){
	reg0 = (sbox[(reg0 & 0xff00)>>8]<<8) | sbox[(reg0&0xff)];
	reg1 = (sbox[(reg1 & 0xff00)>>8]<<8) | sbox[(reg1&0xff)];
	reg2 = (sbox[(reg2 & 0xff00)>>8]<<8) | sbox[(reg2&0xff)];
	reg3 = (sbox[(reg3 & 0xff00)>>8]<<8) | sbox[(reg3&0xff)];
	reg4 = (sbox[(reg4 & 0xff00)>>8]<<8) | sbox[(reg4&0xff)];
	reg5 = (sbox[(reg5 & 0xff00)>>8]<<8) | sbox[(reg5&0xff)];
	reg6 = (sbox[(reg6 & 0xff00)>>8]<<8) | sbox[(reg6&0xff)];
	reg7 = (sbox[(reg7 & 0xff00)>>8]<<8) | sbox[(reg7&0xff)];
}

void isLayer(){
	reg0 = (isbox[(reg0 & 0xff00)>>8]<<8) | isbox[(reg0&0xff)];
	reg1 = (isbox[(reg1 & 0xff00)>>8]<<8) | isbox[(reg1&0xff)];
	reg2 = (isbox[(reg2 & 0xff00)>>8]<<8) | isbox[(reg2&0xff)];
	reg3 = (isbox[(reg3 & 0xff00)>>8]<<8) | isbox[(reg3&0xff)];
	reg4 = (isbox[(reg4 & 0xff00)>>8]<<8) | isbox[(reg4&0xff)];
	reg5 = (isbox[(reg5 & 0xff00)>>8]<<8) | isbox[(reg5&0xff)];
	reg6 = (isbox[(reg6 & 0xff00)>>8]<<8) | isbox[(reg6&0xff)];
	reg7 = (isbox[(reg7 & 0xff00)>>8]<<8) | isbox[(reg7&0xff)];
}

void ishiftrow(){
	uint16_t tmp = reg0;
	reg0 = ((reg0 & 0xf0f0) >> 4) | (reg1 & 0xf0f0); // 7 clocks
	reg1 = ((reg1 & 0x0f0f) << 4) | (tmp & 0x0f0f); // 7 clocks

	reg2 = ((reg2 >> 4) & 0x0f0f) | ((reg2<<4) & 0xf0f0); // it's now reg5, 4+1+1+4+1 = 11 clocks
	reg3 = ((reg3 >> 4) & 0x0f0f) | ((reg3<<4) & 0xf0f0); // it's now reg4

	reg2 = reg2 ^ reg3;
	reg3 = reg2 ^ reg3;
	reg2 = reg2 ^ reg3;

	tmp = reg4;
	reg4 = (reg5 & 0x0f0f) | ((reg4 & 0x0f0f)<<4); // 1+1+1+4 = 7 clocks
	reg5 = (tmp & 0xf0f0) | ((reg5 & 0xf0f0)>>4); // 7 clocks
}

void shiftrow(){
	uint16_t tmp = reg0;
	reg0 = ((reg0 & 0x0f0f) << 4) | (reg1 & 0x0f0f); // 7 clocks
	reg1 = ((reg1 & 0xf0f0) >> 4) | (tmp & 0xf0f0); // 7 clocks

	reg2 = ((reg2 >> 4) & 0x0f0f) | ((reg2<<4) & 0xf0f0); // it's now reg5, 4+1+1+4+1 = 11 clocks
	reg3 = ((reg3 >> 4) & 0x0f0f) | ((reg3<<4) & 0xf0f0); // it's now reg4

	reg2 = reg2 ^ reg3;
	reg3 = reg2 ^ reg3;
	reg2 = reg2 ^ reg3;

	tmp = reg4;
	reg4 = (reg5 & 0xf0f0) | ((reg4 & 0xf0f0)>>4); // 1+1+1+4 = 7 clocks
	reg5 = (tmp & 0x0f0f) | ((reg5 & 0x0f0f)<<4); // 7 clocks
}

void mpLayer(){
	uint16_t tmp0 = reg0 ^ reg2 ^ reg4;
	uint16_t tmp6 = reg6 ^ reg2 ^ reg4;
	uint16_t tmp4 = reg0 ^ reg6 ^ reg4;
	uint16_t tmp2 = reg0 ^ reg2 ^ reg6;

	reg0 = (tmp0 & 0x1111) | (tmp6 & 0x2222) | (tmp4 & 0x4444) | (tmp2 & 0x8888);
	reg2 = (tmp2 & 0x1111) | (tmp0 & 0x2222) | (tmp6 & 0x4444) | (tmp4 & 0x8888);
	reg4 = (tmp4 & 0x1111) | (tmp2 & 0x2222) | (tmp0 & 0x4444) | (tmp6 & 0x8888);
	reg6 = (tmp6 & 0x1111) | (tmp4 & 0x2222) | (tmp2 & 0x4444) | (tmp0 & 0x8888);

	uint16_t tmp3 = reg1 ^ reg3 ^ reg5;
	uint16_t tmp1 = reg3 ^ reg5 ^ reg7;
	uint16_t tmp7 = reg1 ^ reg5 ^ reg7;
	uint16_t tmp5 = reg1 ^ reg3 ^ reg7;

	reg1 = (tmp1 & 0x1111) | (tmp7 & 0x2222) | (tmp5 & 0x4444) | (tmp3 & 0x8888);
	reg3 = (tmp3 & 0x1111) | (tmp1 & 0x2222) | (tmp7 & 0x4444) | (tmp5 & 0x8888);
	reg5 = (tmp5 & 0x1111) | (tmp3 & 0x2222) | (tmp1 & 0x4444) | (tmp7 & 0x8888);
	reg7 = (tmp7 & 0x1111) | (tmp5 & 0x2222) | (tmp3 & 0x4444) | (tmp1 & 0x8888);
}

