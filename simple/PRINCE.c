#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>

uint64_t mLayer(uint64_t state);
uint64_t sLayer(uint64_t state);
uint64_t isLayer(uint64_t state);
uint64_t shiftrow(uint64_t state);
uint64_t ishiftrow(uint64_t state);
uint64_t mpLayer(uint64_t state);
uint64_t mLayer(uint64_t state);
uint64_t imLayer(uint64_t state);
uint64_t Prince(uint64_t key0, uint64_t key1, uint64_t ptext);


#define ComputeK1Prime(k0) (((k0)>>1 | (k0)<<63 ) ^ (k0)>>63)
#define Round0(state, k0, k1) ((state)^(k0)^(k1))


const uint64_t RC[12] = {
		0x0000000000000000,
        0x13198a2e03707344,
        0xa4093822299f31d0,
        0x082efa98ec4e6c89,
        0x452821e638d01377,
        0xbe5466cf34e90c6c,
        0x7ef84f78fd955cb1,
        0x85840851f1ac43aa,
        0xc882d32f25323c54,
        0x64a51195e0e3610d,
        0xd3b5a399ca0c2399,
        0xc0ac29b7c97c50dd
};

int main(int argc, char **argv){
	uint64_t ptext = 0x0123456789abcdef;
	uint64_t key1 =0;
	uint64_t key0 = 0;

	ptext = Prince(key0, key1, ptext);
	printf("0x%016"PRIx64"",ptext);

	return 0;


}

uint64_t Prince(uint64_t key0, uint64_t key1, uint64_t ptext)
{
	int i;
	uint64_t key0p = ComputeK1Prime(key0);
	uint64_t iState = Round0(ptext, key0, key1);

	for(i=1; i<6;i++){
		iState = sLayer(iState);
		iState = mLayer(iState);
		iState = iState^RC[i]^key1;
	}
	iState = sLayer(iState);
	iState = mpLayer(iState);
	iState = isLayer(iState);

	for(i=6; i<11;i++){
		iState = iState^RC[i]^key1;
		iState = imLayer(iState);
		iState = isLayer(iState);
	}
	iState = iState^RC[11]^key1^key0p;

	return iState;

}

uint64_t sLayer(uint64_t state){
	int i;
	uint64_t tmp=0;
	static const uint8_t sbox[16] = {
			0xB, 0xF, 0x3, 0x2,
			0xA, 0xC, 0x9, 0x1,
			0x6, 0x7, 0x8, 0x0,
			0xE, 0x5, 0xD, 0x4};
	for(i=0; i<16; i++){
		tmp |= ((uint64_t)(sbox[(state>>(i*4))&0xf])) << (i*4);
	}
	return tmp;
}

uint64_t isLayer(uint64_t state){
	int i;
	uint64_t tmp=0;
	static const uint8_t isbox[16] = {
			0xB, 0x7, 0x3, 0x2,
			0xF, 0xD, 0x8, 0x9,
			0xA, 0x6, 0x4, 0x0,
			0x5, 0xE, 0xC, 0x1};
	for(i=0; i<16; i++){
		tmp |= ((uint64_t)(isbox[(state>>(i*4))&0xf])) << (i*4);
	}
	return tmp;
}

uint64_t shiftrow(uint64_t state){
	int i;
	uint64_t tmp=0;
	static const uint8_t sr[16] ={
			0x0, 0x5, 0xA, 0xF,
			0x4, 0x9, 0xE, 0x3,
			0x8, 0xD, 0x2, 0x7,
			0xC, 0x1, 0x6, 0xB};
	for(i=0; i<16; i++){
			tmp |= (((state>>((15-sr[15-i])*4))&0xf)<< (i*4));
		}

	return tmp;
}

uint64_t ishiftrow(uint64_t state){
	int i;
	uint64_t tmp=0;
	static const uint8_t isr[16] ={
			0x0, 0xD, 0xA, 0x7,
			0x4, 0x1, 0xE, 0xB,
			0x8, 0x5, 0x2, 0xF,
			0xC, 0x9, 0x6, 0x3};
	for(i=0; i<16; i++){
			tmp |= (((state>>((15-isr[15-i])*4))&0xf)<< (i*4));
		}

	return tmp;
}


uint64_t mpLayer(uint64_t state){
	int i;
	uint64_t tmp1=0;
	uint64_t tmp=0;
	static const uint64_t m64[64] = {
			0x0888000000000000, 0x4044000000000000, 0x2202000000000000,
			0x1110000000000000, 0x8880000000000000, 0x0444000000000000,
			0x2022000000000000, 0x1101000000000000, 0x8808000000000000,
			0x4440000000000000, 0x0222000000000000, 0x1011000000000000,
			0x8088000000000000, 0x4404000000000000, 0x2220000000000000,
			0x0111000000000000, 0x0000888000000000, 0x0000044400000000,
			0x0000202200000000, 0x0000110100000000, 0x0000880800000000,
			0x0000444000000000, 0x0000022200000000, 0x0000101100000000,
			0x0000808800000000, 0x0000440400000000, 0x0000222000000000,
			0x0000011100000000, 0x0000088800000000, 0x0000404400000000,
			0x0000220200000000, 0x0000111000000000, 0x0000000088800000,
			0x0000000004440000, 0x0000000020220000, 0x0000000011010000,
			0x0000000088080000, 0x0000000044400000, 0x0000000002220000,
			0x0000000010110000, 0x0000000080880000, 0x0000000044040000,
			0x0000000022200000, 0x0000000001110000, 0x0000000008880000,
			0x0000000040440000, 0x0000000022020000, 0x0000000011100000,
			0x0000000000000888, 0x0000000000004044, 0x0000000000002202,
			0x0000000000001110, 0x0000000000008880, 0x0000000000000444,
			0x0000000000002022, 0x0000000000001101, 0x0000000000008808,
			0x0000000000004440, 0x0000000000000222, 0x0000000000001011,
			0x0000000000008088, 0x0000000000004404, 0x0000000000002220,
			0x0000000000000111};

	for(i=0; i<64;i++){
		tmp = state & m64[i];
		tmp ^= tmp>>32;
		tmp ^= tmp>>16;
		tmp ^= tmp>>8;
		tmp ^= tmp>>4;
		tmp ^= tmp>>2;
		tmp ^= tmp>>1;
		tmp1 |= ((tmp&0x1)<<(63-i));
	}
	return tmp1;
}

uint64_t mLayer(uint64_t state){
	state = mpLayer(state);
	return shiftrow(state);
}

uint64_t imLayer(uint64_t state){
	state = ishiftrow(state);
	return mpLayer(state);
}
